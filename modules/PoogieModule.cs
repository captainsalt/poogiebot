using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using System.Linq;

namespace PoogieBot.Modules
{
    public class PoogieModule : ModuleBase
    {
        private readonly List<string> weapons = File.ReadAllLines("data/weapons.txt").ToList();
        private readonly List<string> styles = File.ReadAllLines("data/styles.txt").ToList();
        private readonly List<string> monsters = File.ReadAllLines("data/monsters.txt").ToList();
        private readonly Random rng = new Random();

        [Command("poogie")]
        [Alias("p", "{og")]
        public async Task Poogie()
        {
            var replyBuilder = new StringBuilder();
            replyBuilder.AppendLine(RandomMonster());
            replyBuilder.AppendLine();

            for (var i = 0; i < 4; i++)
                replyBuilder.AppendLine(RandomWeaponStyle());

            await ReplyAsync(replyBuilder.ToString());
        }

        // Use user config to re-roll in the future
        [Command("roll")]
        [Alias("r")]
        public async Task Roll() =>
            await ReplyAsync(RandomWeaponStyle());

        [Command("rollmonster")]
        [Alias("rm")]
        public async Task RollMonster() =>
            await ReplyAsync(RandomMonster());

        private string RandomWeaponStyle() =>
            $"{RandomItem(styles)} {RandomItem(weapons)}";

        private string RandomMonster() =>
            RandomItem(monsters);

        private string RandomItem(IList<string> collection) =>
            collection[rng.Next(collection.Count)];
    }
}