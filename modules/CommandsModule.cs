using Discord.Commands;
using System.Threading.Tasks;
using System.Text;
using System.Collections;
using System.Linq;
using Discord;
using System;

namespace PoogieBot.Modules
{
    public class Commands : ModuleBase
    {
        public CommandService CommandService { get; set; }

        [Command("commands")]
        [Alias("c", "cmds")]
        async Task CommandsAsync()
        {
            var commands = CommandService.Commands;
            var commandBuilder = new StringBuilder();
            var largestCmd = commands
                .Select(cmd => cmd.Name)
                .Aggregate((maxStr, current) => (current.Length > maxStr.Length) ? current : maxStr)
                .Length;

            foreach (var cmd in commands)
            {
                var aliases = string.Join(", ", cmd.Aliases.Skip(1));

                if (cmd.Aliases.Count() > 1)
                    commandBuilder.AppendLine(string.Format($"{{0, {-largestCmd}}} ⇄ {{1}}", cmd.Name, aliases));
                else
                    commandBuilder.AppendLine(cmd.Name);
            }

            await ReplyAsync(Format.Code(commandBuilder.ToString()));
        }
    }
}
