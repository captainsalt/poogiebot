using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.IO;

namespace PoogieBot.Services
{
    class StartupService
    {
        private readonly IServiceProvider _provider;
        private readonly DiscordSocketClient _client;

        public StartupService(
           IServiceProvider provider,
           DiscordSocketClient client)
        {
            _provider = provider;
            _client = client;
        }

        public async Task RunAsync()
        {
            var token = await GetToken();
            await _client.LoginAsync(TokenType.Bot, token.Trim());
            await _client.StartAsync();
        }

        private async Task<string> GetToken()
        {
            var dockerSecretPath = Environment.GetEnvironmentVariable("DISCORD_TOKEN_PATH");

            if (File.Exists(dockerSecretPath))
                return await File.ReadAllTextAsync(dockerSecretPath);

            return Environment.GetEnvironmentVariable("DISCORD_TOKEN") ?? await File.ReadAllTextAsync("token.txt");
        }
    }
}