﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using System.IO;
using Discord.Commands;
using Microsoft.Extensions.DependencyInjection;
using PoogieBot.Services;

namespace PoogieBot
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var provider = BuildServiceProvider();

            provider.GetRequiredService<LoggingService>();
            await provider.GetRequiredService<CommandHandler>().InstallCommandsAsync();
            await provider.GetRequiredService<StartupService>().RunAsync();

            await Task.Delay(-1);
        }

        private static ServiceProvider BuildServiceProvider() =>
            new ServiceCollection()
                .AddSingleton(new DiscordSocketClient())
                .AddSingleton(new CommandService())
                .AddSingleton<CommandHandler>()
                .AddSingleton<StartupService>()
                .AddSingleton<LoggingService>()
                .BuildServiceProvider();
    }
}